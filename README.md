# Starterpack RUST

Petit projet bac-à-sable pour apprendre les rudiments de RUST

Pourquoi Rust ? 

- langage bas niveau 
- langage récent (first release en 2015)
- semble gagner pas mal en popularité depuis ces dernières années
- langage [qui taquine côté stats](https://greenlab.di.uminho.pt/wp-content/uploads/2017/10/sleFinal.pdf)

Je m'appuie [sur ce repo/tuto](https://jimskapt.github.io/rust-book-fr/ch03-01-variables-and-mutability.html) qui sert de tuto pour rust

Commandes pour compiler et exécuter un fichier rust arbitraire (sans passer par cargo qui pointe automatiquement sur src/main.rs) : 
```
rustc rust/les_bases/les_variables.rs -o target/bin
./target/bin
```

## Initialisation du repo 

- il faut avoir installé rust et cargo
- puis il faut lancer un `cargo install --path .`
- épicétou !

## Sommaire

0. [Cargo (gestionnaire de package rust) : les bases](cargo/les_bases/README.md)
1. [Rust : les bases](rust/les_bases/README.md)