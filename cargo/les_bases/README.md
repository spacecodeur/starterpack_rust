# Cargo : les bases

Cargo semble fonctionner de la même manière qu'un npm ou encore composer. 
Plutôt chouette de pouvoir taffer sur un langage bas level tout en profitant des mécanismes d'un gestionnaire de package moderne

Initiatiser un projet : `cargo init`
Ajouter des packages : `cargo add ...`
Installer les dépendances : `cargo install`