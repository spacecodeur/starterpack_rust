fn main() 
{
    /*
    * variable type scalaire : une seule valeur
    */

    // entier : nombre sans partie décimale

    integer_strictement_positif();// entiers NON signés
    integer_negatif_ou_positif(); // entiers signés
    litteraux_numerique(); // decimal, octal, hexa, ...

    // nombre flottant
    float();

    // boolean
    bool();

    // char + string
    char_string();


    /*
    * variable type composée : un ensemble de valeurs
    */
}


fn char_string()
{
    let a: char = 'a';
    // let b: String = "bbb"; <= fonctionne pas
    let b: &str = "bbb";

    println!("{} {}", a, b);

    // let c: char = "c"; <- pas bon, "c" sera considéré comme un string par défault, et donc ça va couiller avec char
    // let d: string = 'ddd'; <- ne fonctionne pas, il faut des " pour les strings
}

fn bool()
{
    let t = true;
    let f: bool = false;
    
    println!("{} {}", t, f);
}

fn float()
{
    let x = 2.0; // f64
    let y: f32 = 3.0; // f32

    println!("{} {}", x, y);
}

fn integer_strictement_positif() // non signé
{
    let a: u8 = 10;
    let b: u16 = 11;
    let c: u32 = 12;
    let d: u64 = 13;
    let e: u128 = 14;
    let f: usize = 15;

    println!("{} {} {} {} {} {}", a, b, c, d, e, f);
}

fn integer_negatif_ou_positif() // signé
{
    let a: i8 = -10;
    let b: i16 = 11;
    let c: i32 = -12;
    let d: i64 = 13;
    let e: i128 = 14;
    let f: isize = -15;

    println!("{} {} {} {} {} {}", a, b, c, d, e, f);

}

fn litteraux_numerique() // decimal, octal, hexa, ...
{
    let octal_unsigned: u32 = 0o123; 
    let octal_signed: i32 = -0o123; 

    let hexa_unsigned: u32 = 0xaa;
    let hexa_signed: i32 = -0xaa;

    let binary_unsigned: u8 = 0b0000_0110;
    let binary_signed: i8 = -0b0110;

    println!("{} {} {} {} {} {}", octal_unsigned, octal_signed, hexa_unsigned, hexa_signed, binary_unsigned, binary_signed);

    // octet => seulement en unsigned 8
    let octet_unsigned1: u8 = b'A'; // correspond à la correspondance de A en table d'ascii
    let octet_unsigned2: u8 = b'a';
    let octet_unsigned3: u8 = b'9';
    let octet_unsigned4: u8 = b'~';

    
    println!("{} {} {} {}", octet_unsigned1, octet_unsigned2, octet_unsigned3, octet_unsigned4);
}