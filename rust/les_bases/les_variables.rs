const TRUC: u32 = 99; // sera supplenté par TRUC dans constante()

fn main() 
{
    println!("Exemples de code pour la partie : {}", "variable");

    variable_mutable_or_not();
    
    // const TRUC: u32 = 60 * 60 * 3; ne sera pas accessible dans constante()
    println!("{}", TRUC);
    constante();

    masquage_de_variable();
}

fn variable_mutable_or_not()
{
    let _plop = 17; // pourquoi le _ ? pour confirmer que la variable plop est volontairement unused
    // plop = 18; <- planterai car par défault plop est non mutable

    let mut _plip = 10;
    _plip = 11; // <- par contre là ça marche, car plip est mutable
}

fn constante()
{
    // (of course) on ne peux pas use mut avec les constantes
    const TRUC: u32 = 60 * 60 * 3;
    println!("{}", TRUC);
}

fn masquage_de_variable()
{
    let _pouet = 17; // malgré le fait que _pouet soit non mutable, on peut le masquer...
    let _pouet = "    "; // puis changer son type dans la foulée
    let _pouet = _pouet.len();

    println!("{}", _pouet);

    // par contre, il n'est pas possible de changer de la meme maniere le type d'une variable mutable
    let mut _plop = "    ";
    // _plop = _plop.len(); <- déclenche une erreur mismatched types   
}